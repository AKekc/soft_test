<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 02.01.2017
 * Time: 16:12
 */
session_start();
require_once("posts.php");

?>
<!DOCTYPE HTML>

<html>
<head>
    <title>Striped by HTML5 UP</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-desktop.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->


    <?php




    $views = $view['view']+1;
    $stmt = $db->prepare("UPDATE posts SET view = :view WHERE id=:id");
    $stmt->bindParam(':view', $views, PDO::PARAM_STR);
    $stmt->bindParam(':id', $post['id'], PDO::PARAM_INT);
    $stmt->execute();

    ?>
</head>

<body class="left-sidebar">

<!-- Wrapper -->
<div id="wrapper">

    <!-- Content -->
    <div id="content">
        <div class="inner">

            <!-- Post -->


                <article class="box post post-excerpt">
                    <header>


                        <h2><a href="single.php?id=<?php  echo $post['id']; ?>"> <?php  echo $post['title']; ?> </a></h2>

                    </header>
                    <div class="info">

                    <span class="date">
                        <span class="month"><?php  echo $post['date']; ?></span>
                    </span>
                        <!--
                            Note: You can change the number of list items in "stats" to whatever you want.
                        -->
                        <ul class="stats">
                            <li><span href="#" class="icon fa-eercast">
                                <?php
                                echo $row['view'];
                                ?>

                            </span></li>

                        </ul>
                    </div>
                    <span href="single.php?id=<?php  echo $row['id']; ?>" class="image featured">
                        <?php  echo $row['small']; ?>
                    </span>
                    <p>
                        <?php  echo $post['content']; ?>
                    </p>
                </article>


        </div>
    </div>

    <!-- Sidebar -->
    <div id="sidebar">

        <!-- Logo -->
        <h1 id="logo"><a href="/">SG Blog</a></h1>

        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li class="current"><a href="#">Все посты</a></li>

                <li><?if (empty( $_SESSION['name'])) {?>	<a  href="#myModal2"  data-toggle="modal" class="link login" id="reg" >Регистрация</a></li>
                <li><a href="#myModal"  data-toggle="modal" class="link login" id="go" >Вход</a></li>
                <?}else{?>

                    <li>
                        <a class="link login" href="/exit.php" style="margin-right: 50px;">Выход</a>
                    </li>
                <?}?>

            </ul>
        </nav>




        <!-- Calendar -->
        <section class="box calendar">
            <div class="inner">
                <table>
                    <caption>July 2014</caption>
                    <thead>
                    <tr>
                        <th scope="col" title="Monday">M</th>
                        <th scope="col" title="Tuesday">T</th>
                        <th scope="col" title="Wednesday">W</th>
                        <th scope="col" title="Thursday">T</th>
                        <th scope="col" title="Friday">F</th>
                        <th scope="col" title="Saturday">S</th>
                        <th scope="col" title="Sunday">S</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="4" class="pad"><span>&nbsp;</span></td>
                        <td><span>1</span></td>
                        <td><span>2</span></td>
                        <td><span>3</span></td>
                    </tr>
                    <tr>
                        <td><span>4</span></td>
                        <td><span>5</span></td>
                        <td><a href="#">6</a></td>
                        <td><span>7</span></td>
                        <td><span>8</span></td>
                        <td><span>9</span></td>
                        <td><a href="#">10</a></td>
                    </tr>
                    <tr>
                        <td><span>11</span></td>
                        <td><span>12</span></td>
                        <td><span>13</span></td>
                        <td class="today"><a href="#">14</a></td>
                        <td><span>15</span></td>
                        <td><span>16</span></td>
                        <td><span>17</span></td>
                    </tr>
                    <tr>
                        <td><span>18</span></td>
                        <td><span>19</span></td>
                        <td><span>20</span></td>
                        <td><span>21</span></td>
                        <td><span>22</span></td>
                        <td><a href="#">23</a></td>
                        <td><span>24</span></td>
                    </tr>
                    <tr>
                        <td><a href="#">25</a></td>
                        <td><span>26</span></td>
                        <td><span>27</span></td>
                        <td><span>28</span></td>
                        <td class="pad" colspan="3"><span>&nbsp;</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>

        <!-- Copyright -->
        <ul id="copyright">
            <li>&copy; Untitled.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>

    </div>

</div>

</body>
</html>
