<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 02.01.2017
 * Time: 17:19
 */

require_once("posts.php");
session_start();
?>

<!DOCTYPE HTML>

<html>
<head>
    <title>Striped by HTML5 UP</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-desktop.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
    </noscript>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="https://use.fontawesome.com/13cebad2b0.js"></script>
    <link rel="stylesheet" href="/css/style.css" type="text/css" charset="utf-8"/>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>


<!-- HTML-код модального окна -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->

            <!-- Основное содержимое модального окна -->
            <div class="body-modal">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Авторизация на сайте</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-12 login-box">
                                        <form id='logform' role="form">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                <input name="name" type="text" class="form-control" placeholder="Имя пользователя" required autofocus />
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                                <input name="pass" type="password" class="form-control" placeholder="Ваш пароль" required />
                                            </div>



                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <p>

                                        </p>
                                        <button  id="log_btn" type="button" id="log_btn" class="btn btn-labeled btn-success">
                                            <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Войти</button>
                                        <button type="button" class="btn btn-labeled btn-danger"  data-dismiss="modal">
                                            <span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>Закрыть</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <!-- Футер модального окна -->

        </div>
    </div>
</div>

<!--Конец формы-->


<!-- HTML-код модального окна -->
<div id="myModal2" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->

            <!-- Основное содержимое модального окна -->
            <div class="body-modal">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Регистрация на сайте</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-12 login-box">
                                        <form id="regform" role="form">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                <input name="name" type="text" class="form-control" placeholder="Имя пользователя" required autofocus />
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                                <input name="sec_name" type="text" class="form-control" placeholder="Фамилия пользователя" required autofocus />
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-envelope"></span></span>

                                                <input name="mail" type="text" class="form-control" placeholder="Почта" required autofocus />
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-mobile fa-2"></span></span>

                                                <input name="tel" type="text" class="form-control" placeholder="Телефон" required autofocus />
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                                <input name="pass" type="password" class="form-control" placeholder="Ваш пароль" required />
                                            </div>



                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <button id="reg_btn" type="button" class="btn btn-labeled btn-success">
                                            <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Зарегистрироваться</button>
                                        <button type="button" class="btn btn-labeled btn-danger"  data-dismiss="modal">
                                            <span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>Закрыть</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <!-- Футер модального окна -->

        </div>
    </div>
</div>
<!--Конец формы-->


<script>
    $('#log_btn').click(function() {
        $.ajax({
            type: "POST",
            url: "testreg.php",
            data: $('#logform').serialize(),

            success: function(resp) {
                if (resp == 'admin') {
                    window.location.href='/admin';
                } else if (resp == 'user') {
                    window.location.href='/';
                } else alert(resp);
            },
            error: function(resp) {
                alert('error');
            }
        });
        return false;
    });
</script>

<script>
    $('#reg_btn').click(function() {
        $.ajax({
            type: "POST",
            url: "save_user.php",
            data: $('#regform').serialize(),

            success: function(resp) {
                if (resp == 'yes') {
                    alert('yes');
                    $('#logform .form_err:first').css('display', 'none');
                    $('.err_true').css('display', 'block');
                } else if (resp == 'no') {
                    alert('no');
                    $('#logform .form_err:first').css('display', 'block');
                } else alert(resp);
            },
            error: function(resp) {
                alert('error');
            }
        });
        return false;
    });
</script>