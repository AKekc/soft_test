
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Пользователи</title>

	<?php
	include ("header.php");
	?>
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Lumino</span>Pro</a>

			</div>
		</div><!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

		<ul class="nav menu">
			<li ><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Главная</a></li>
			<li ><a href="posts.php"><span class="glyphicon glyphicon-th"></span> Записи</a></li>

			<li class="active"><a href="tables.php"><span class="glyphicon glyphicon-list-alt"></span> Пользователи</a></li>

			<li role="presentation" class="divider"></li>
			<li><a href="login.php"><span class="glyphicon glyphicon-user"></span> Выход</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Пользователи</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Пользователи</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Таблица пользователей
						<a href="add_user.php" class="btn btn-primary" >Добавить</a>
					</div>
					<div class="panel-body">
						<div class="bootstrap-table">

							<div class="fixed-table-container">
								<div class="fixed-table-header">
									<table></table>
								</div>
								<div class="fixed-table-body">


									<table  class="table table-hover">
									<thead>
										<tr>

											<th style="">
												<div class="th-inner sortable">ID Пользователя</div>
												<div class="fht-cell"></div>
											</th>
											<th style="">
												<div class="th-inner sortable">Имя Пользователя<span class="order">
														<span class="caret" style="margin: 10px 5px;"></span>
													</span>
												</div>
												<div class="fht-cell"></div>
											</th>
											<th style="">
												<div class="th-inner sortable">Фамилия Пользователя</div>
												<div class="fht-cell"></div>
											</th>
											<th style="">
												<div class="th-inner sortable">Телефон Пользователя</div>
												<div class="fht-cell"></div>
											</th>
											<th style="">
												<div class="th-inner sortable">Почта Пользователя</div>
												<div class="fht-cell"></div>
											</th>
                                            <th style="">
                                                <div class="th-inner sortable">Пароль Пользователя</div>
                                                <div class="fht-cell"></div>
                                            </th>
                                            <th style="">
                                                <div class="th-inner sortable">Роль Пользователя</div>
                                                <div class="fht-cell"></div>
                                            </th>
											<th style="">
												<div class="th-inner sortable">Действия</div>
												<div class="fht-cell"></div>
											</th>
										</tr>
										</thead>
										<tbody>
										<?php

										foreach($stmt = $db->query('SELECT * FROM users') as $row) {
											?>

										<tr>

											<td style=""><?php  echo $row['id']; ?></td>
											<td style=""><?php  echo $row['name']; ?></td>
											<td style=""><?php  echo $row['sec_name']; ?></td>
											<td style=""><?php  echo $row['tel']; ?></td>
                                            <td style=""><?php  echo $row['mail']; ?></td>
                                            <td style=""><?php  echo $row['pass']; ?></td>

                                            <td style=""><?php  echo $row['role']; ?></td>
											<td><a class="btn btn-warning" href="user.php?id=<?php  echo $row['id']; ?>"> Изменить</a>

                                            <a class="btn btn-danger" href="delete_user.php?id=<?php  echo $row['id']; ?>"> Удалить</a>
                                            </td>




										</tr>

										<?php } ?>

										</tbody>
									</table>

								</div>

							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div><!--/.row-->	

		</div><!--/.row-->	
		
		
	</div><!--/.main-->

	<?php
	include ("footer.php");
	?>
</body>

</html>
