<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 04.01.2017
 * Time: 18:58
 */

include ("../connect.php");

$stmt = $db->prepare("UPDATE posts SET title = :title, small = :small, content = :content WHERE id=:id");
$stmt->bindParam(':title', $_POST['title'], PDO::PARAM_STR);
$stmt->bindParam(':small', $_POST['small'], PDO::PARAM_STR);
$stmt->bindParam(':content', $_POST['content'], PDO::PARAM_STR);
$stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
$stmt->execute();

echo "Вы успешно отредактировали  пост! ";

?>