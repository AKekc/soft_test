<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 04.01.2017
 * Time: 17:50
 */

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Редактирование поста</title>

    <?php
    include("header.php");
    ?>
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>Lumino</span>Pro</a>

        </div>
    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

    <ul class="nav menu">
        <li ><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Главная</a></li>
        <li class="active"><a href="posts.php"><span class="glyphicon glyphicon-th"></span> Записи</a></li>
        <li ><a href="charts.php"><span class="glyphicon glyphicon-stats"></span> Статистика</a></li>
        <li ><a href="tables.php"><span class="glyphicon glyphicon-list-alt"></span> Пользователи</a></li>

        <li role="presentation" class="divider"></li>
        <li><a href="login.php"><span class="glyphicon glyphicon-user"></span> Выход</a></li>
    </ul>
</div><!--/.sidebar-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Tables</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Изменение поста</h1>
        </div>
    </div><!--/.row-->


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Информация поста</div>
                <div class="panel-body">
                    <div class="bootstrap-table">

                        <div class="fixed-table-container">
                            <div class="fixed-table-header">
                                <table></table>
                            </div>
                            <div class="fixed-table-body">

                                <form action="update_post.php" method="post">
                                    <input type="hidden" name="id" value="<?=$_GET['id'];?>">
                                <table data-toggle="table" data-url="tables/data1.json" data-show-refresh="true"
                                       data-show-toggle="true" data-show-columns="true" data-search="true"
                                       data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name"
                                       data-sort-order="desc" class="table table-hover">
                                    <thead>
                                    <tr>

                                        <th style="">
                                            <div class="th-inner sortable">ID Поста</div>
                                            <div class="fht-cell"></div>
                                        </th>
                                        <th style="">
                                            <div class="th-inner sortable">Название Поста<span class="order">
														<span class="caret" style="margin: 10px 5px;"></span>
													</span>
                                            </div>
                                            <div class="fht-cell"></div>
                                        </th>
                                        <th style="">
                                            <div class="th-inner sortable">Краткое содержимое Поста</div>
                                            <div class="fht-cell"></div>
                                        </th>
                                        <th style="">
                                            <div class="th-inner sortable">Контент Поста</div>
                                            <div class="fht-cell"></div>
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    <tr >

                                        <td style="">

                                            <?php  echo $posts_edit['id']; ?>

                                        </td>
                                        <td style="">
                                            <textarea name="title" >
                                            <?php  echo $posts_edit['title']; ?>
                                                 </textarea>
                                        </td>
                                        <td style="">
                                            <textarea name="small"  cols="30" rows="10">
                                            <?php  echo $posts_edit['small']; ?>
                                                 </textarea>

                                        </td>

                                        <td style=""> <textarea name="content"  cols="30" rows="10">
                                            <?php  echo $posts_edit['content']; ?>
                                                 </textarea></td>




                                    </tr>



                                    </tbody>
                                </table>

                                <input type="submit" id="save_btn" class="btn btn-primary" name="submit" value="Сохранить">
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div><!--/.row-->


</div><!--/.main-->

<?php
include ("footer.php");
?>
</body>

</html>

