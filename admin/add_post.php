<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 04.01.2017
 * Time: 18:02
 */
?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Пользователи</title>

    <?php
    include ("header.php");
    ?>
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>Lumino</span>Pro</a>

        </div>
    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

    <ul class="nav menu">
        <li ><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Главная</a></li>
        <li class="active"><a href="posts.php"><span class="glyphicon glyphicon-th"></span> Записи</a></li>
        <li ><a href="charts.php"><span class="glyphicon glyphicon-stats"></span> Статистика</a></li>
        <li ><a href="tables.php"><span class="glyphicon glyphicon-list-alt"></span> Пользователи</a></li>

        <li role="presentation" class="divider"></li>
        <li><a href="login.php"><span class="glyphicon glyphicon-user"></span> Выход</a></li>
    </ul>
</div><!--/.sidebar-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Tables</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Добавление поста</h1>
        </div>
    </div><!--/.row-->


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Добавить пост


                </div>
                <div class="panel-body">
                    <div class="bootstrap-table">

                        <div class="fixed-table-container">
                            <div class="fixed-table-header">

                            </div>
                            <div class="fixed-table-body">

                                <form action="save_post.php" method="post">

                                    <p>
                                        <label>Название поста:<br></label>
                                        <textarea name="title"  >
                                            </textarea>
                                    </p>

                                    <p>
                                        <label>Краткое описание:<br></label>
                                        <textarea name="small" >
                                            </textarea>
                                    </p>
                                    <p>
                                        <label>Содержание:<br></label>
                                        <textarea name="content">
                                            </textarea>
                                    </p>


                                    <p>
                                        <input type="submit" id="save_btn" class="btn btn-primary" name="submit" value="Добавить">

                                    </p>
                                </form>

                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div><!--/.row-->


</div><!--/.main-->


<?php
include ("footer.php");
?>
</body>

</html>

