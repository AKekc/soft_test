<?php session_start(); if($_SESSION['id'] != 1)
	echo "<script>window.location.href='login.php';</script>";

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Панель управления</title>

	<?php
	include ("header.php");
	?>
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Lumino</span>Pro</a>

			</div>
		</div><!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

		<ul class="nav menu">
			<li class="active"><a href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Главная</a></li>
			<li ><a href="posts.php"><span class="glyphicon glyphicon-th"></span> Записи</a></li>

			<li ><a href="tables.php"><span class="glyphicon glyphicon-list-alt"></span> Пользователи</a></li>

			<li role="presentation" class="divider"></li>
			<li><a href="login.php"><span class="glyphicon glyphicon-user"></span> Выход</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active">Главная</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Главная</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row col-no-gutter-container">
			<div class="col-xs-12 col-md-6 col-lg-3 col-no-gutter">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<em class="glyphicon glyphicon-comment glyphicon-l"></em>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">
								<?php echo $count_posts; ?>
							</div>
							<div class="text-muted">Все посты</div>
						</div>
					</div>
				</div>
			</div>


		<div class="row col-no-gutter-container">
			<div class="col-xs-12 col-md-6 col-lg-3 col-no-gutter">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<em class="glyphicon glyphicon-comment glyphicon-l"></em>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">
								<?php echo $all_view['view']; ?>
							</div>
							<div class="text-muted">Все просмотры</div>
						</div>
					</div>
				</div>
			</div>
		</div>



			<div class="col-xs-12 col-md-6 col-lg-3 col-no-gutter">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<em class="glyphicon glyphicon-user glyphicon-l"></em>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large">
								<?php echo $members; ?>
							</div>
							<div class="text-muted">Пользователи</div>
						</div>
					</div>
				</div>
			</div>

		</div><!--/.row-->
		


	</div>	<!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>





</body>

</html>
