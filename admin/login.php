
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>

	<?php
	include ("header.php");
	?>
</head>

<body>

<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form id="logform"  method="post">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="name" name="name" type="text" autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="pass" type="password" value="">
							</div>
							<input type="submit" id="log_btn" class="btn btn-primary" name="submit" value="Войти">
							<!--							<a   class="btn btn-primary">Войти</a>-->
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->


	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>

	<script>
		$('#log_btn').click(function() {
			$.ajax({
				method: "POST",
				url: "../testreg.php",
				data: $('#logform').serialize(),

				success: function(resp) {
					if (resp == 'admin') {
						window.location.href='/admin';
					} else if (resp == 'user') {
						window.location.href='/';
					} else alert(resp);
				},
				error: function(resp) {
					alert('error');
				}
			});
			return false;
		});
	</script>
<?php
include ("footer.php");
?>

</html>
